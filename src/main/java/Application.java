import com.github.javafaker.Faker;
import infrastructure.entity.Student;
import infrastructure.repository.StudentRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    CommandLineRunner commandLineRunner(StudentRepository studentRepository) {
        return args -> studentRepository.saveAll(generateStudents());
    }

    List<Student> generateStudents() {
        Faker faker = new Faker();
        List<Student> students = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            students.add(new Student(faker.name().firstName(), faker.name().lastName(),
                    faker.name().name(), faker.name().name(), UUID.randomUUID().toString()));
        }
        return students;
    }

}
