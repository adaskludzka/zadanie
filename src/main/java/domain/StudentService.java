package domain;

import domain.exception.AppExceptionCode;
import domain.exception.DomainException;
import infrastructure.entity.Student;
import infrastructure.repository.StudentRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class StudentService {
    private final StudentRepository studentRepository;

    public Student create(String firstname, String lastname, String dateOfBirth, String fieldOfStudy) {
        return studentRepository.save(new Student(firstname, lastname, dateOfBirth, fieldOfStudy, UUID.randomUUID().toString()));
    }

    public void delete(String uuid) {

        Student student;
        if (studentRepository.findByUuid(uuid).isPresent()) {
            student = studentRepository.findByUuid(uuid).get();
        } else {
            throw new DomainException(AppExceptionCode.NO_SUCH_STUDENT);
        }

        if(student.isDeleted()){
            throw new DomainException(AppExceptionCode.STUDENT_DELETED);
        }else{
            student.setDeleted(true);
        }

    }

    public  Student getStudentByUuid(String uuid){
        return findStudent(uuid);
    }

    private Student findStudent(String uuid) {
        Student student = studentRepository.findByUuid(uuid).orElseThrow(
                AppExceptionCode.NO_SUCH_STUDENT::createException
        );
        if (student.isDeleted()) {
            throw new DomainException(AppExceptionCode.STUDENT_DELETED);
        } else return student;
    }

    public Student updateStudent(String uuid, String firstname, String lastname, String dateOfBirth, String fieldOfStudy) {
        Student student = findStudent(uuid);
        return studentRepository.save(updateStudent(uuid,firstname,lastname,dateOfBirth,fieldOfStudy));
    }

    public List<Student> getAllStudents() {
        return studentRepository.findAll();
    }



}
