package domain.exception;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum AppExceptionCode {
    NO_SUCH_STUDENT("There is no such student with passed ID", 400),
    STUDENT_DELETED("Student already deleted", 400);

    private final String message;
    private final int status;

    public String getMessage() {
        return message;
    }

    public int getStatus() {
        return status;
    }

    public DomainException createException() {
        return new DomainException(this);
    }
}
