package api.dto;

import lombok.Builder;


@Builder
public class StudentResponse {

    private String firstname;
    private String lastname;
    private String dateOfBirth;
    private String fieldOfStudy;
    private String uuid;

    public StudentResponse() {
    }

    public StudentResponse(String firstname, String lastname, String dateOfBirth, String fieldOfStudy, String uuid) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.dateOfBirth = dateOfBirth;
        this.fieldOfStudy = fieldOfStudy;
        this.uuid = uuid;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getFieldOfStudy() {
        return fieldOfStudy;
    }

    public String getUuid() {
        return uuid;
    }
}
