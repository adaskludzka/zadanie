package api.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class StudentRequest {

    private String firstname;
    private String lastname;
    private String dateOfBirth;
    private String fieldOfStudy;



}
