package api.controller;

import api.dto.StudentRequest;
import api.dto.StudentResponse;
import api.exception.AppExceptionHandler;
import domain.StudentService;
import infrastructure.entity.Student;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/student")
@AllArgsConstructor
public class StudentController {
    private final ConversionService conversionService;
    private final StudentService studentService;

    @PostMapping
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200"),
                    @ApiResponse(responseCode = "500", content = @Content(schema = @Schema(implementation = AppExceptionHandler.Error.class)))
            })
    public ResponseEntity<StudentResponse> createStudent(@Valid @RequestBody StudentRequest studentRequest){
        Student student = studentService.create(studentRequest.getFirstname(),
                studentRequest.getLastname(), studentRequest.getDateOfBirth(), studentRequest.getFieldOfStudy());
        return ResponseEntity.ok((Objects.requireNonNull(conversionService.convert(student, StudentResponse.class))));
    }

    @DeleteMapping
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200"),
                    @ApiResponse(responseCode = "400"),
                    @ApiResponse(responseCode = "500", content = @Content(schema = @Schema(implementation = AppExceptionHandler.Error.class)))
            })
    public ResponseEntity<Void> deleteStudent(@PathVariable String uuid){
        studentService.delete(uuid);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{uuid}")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200"),
                    @ApiResponse(responseCode = "400", content = @Content(schema = @Schema(implementation =
                            AppExceptionHandler.Error.class))),
                    @ApiResponse(responseCode = "500", content = @Content(schema = @Schema(implementation =
                            AppExceptionHandler.Error.class)))
            })
    public ResponseEntity<StudentResponse> getStudentByUuid(@PathVariable String uuid) {
        return ResponseEntity.ok(Objects.requireNonNull(conversionService.convert(
                studentService.getStudentByUuid(uuid), StudentResponse.class)));
    }

    @PutMapping("/{uuid}")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200"),
                    @ApiResponse(responseCode = "400", content = @Content(schema = @Schema(implementation =
                            AppExceptionHandler.Error.class))),
                    @ApiResponse(responseCode = "500", content = @Content(schema = @Schema(implementation =
                            AppExceptionHandler.Error.class)))
            })
    public ResponseEntity<StudentResponse> updateStudent(@PathVariable String uuid,
                                                       @RequestBody StudentRequest studentRequest) {
        return ResponseEntity.ok(Objects.requireNonNull(conversionService.convert(
                studentService.updateStudent(uuid, studentRequest.getFirstname(),
                        studentRequest.getLastname(), studentRequest.getDateOfBirth(), studentRequest.getFieldOfStudy()), StudentResponse.class)));
    }

    @GetMapping("/all")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200"),
                    @ApiResponse(responseCode = "400", content = @Content(schema = @Schema(implementation =
                            AppExceptionHandler.Error.class))),
                    @ApiResponse(responseCode = "500", content = @Content(schema = @Schema(implementation =
                            AppExceptionHandler.Error.class)))
            })
    public ResponseEntity<List<StudentResponse>> getAllStudents() {
        return ResponseEntity.ok(studentService.getAllStudents().stream()
                .map(p -> conversionService.convert(p, StudentResponse.class))
                .collect(Collectors.toList()));
    }

}
