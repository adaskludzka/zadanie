package infrastructure.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity(name = "Student")
@Table(name = "student")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Student {
    @Id
    @SequenceGenerator(
            name = "student_id_sequence",
            sequenceName = "student_id_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator= "student_id_sequence"
    )

    private Long id;
    private String firstname;
    private String lastname;
    private String dateOfBirth;
    private String fieldOfStudy;
    private String uuid;
    private boolean deleted;

    public Student(String firstname, String lastname, String dateOfBirth, String fieldOfStudy, String uuid) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.dateOfBirth = dateOfBirth;
        this.fieldOfStudy = fieldOfStudy;
        this.uuid = uuid;
        this.deleted = false;

    }

    public Long getId() {
        return id;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getUuid() {
        return uuid;
    }

    public String getFieldOfStudy() {
        return fieldOfStudy;
    }

    public boolean isDeleted() {
        return deleted;
    }


    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public void setFieldOfStudy(String fieldOfStudy) {
        this.fieldOfStudy = fieldOfStudy;
    }


    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }


}