package infrastructure.repository;

import infrastructure.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface StudentRepository extends JpaRepository<Student,Long> {

    Optional<Student>findByUuid(String uuid);
}
