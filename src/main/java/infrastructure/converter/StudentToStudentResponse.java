package infrastructure.converter;

import api.dto.StudentResponse;
import infrastructure.entity.Student;
import org.springframework.core.convert.converter.Converter;

public class StudentToStudentResponse implements Converter<Student, StudentResponse> {

    @Override
    public StudentResponse convert(Student student) {
        return StudentResponse.builder()
                .firstname(student.getFirstname())
                .lastname(student.getLastname())
                .dateOfBirth(student.getDateOfBirth())
                .fieldOfStudy(student.getFieldOfStudy())
                .uuid(student.getUuid())
                .build();
    }
}
